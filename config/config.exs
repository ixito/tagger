use Mix.Config

config :maru, Tagger.Server,
  adapter: Plug.Cowboy,
  plug: Tagger.API,
  scheme: :http,
  port: 8880,
  bind_addr: "0.0.0.0"

config :maru,
  maru_servers: [Tagger.Server]

import_config "#{Mix.env}.exs"

