defmodule Tagger.Storage do
  @moduledoc """
  Storage module, using Memento as :mnesia wrapper
  """
  use GenServer
  require Logger
  use Memento.Table,
    attributes: [:id, :envkind, :namespace, :name, :tag],
    index: [:envkind],
    type: :ordered_set,
    autoincrement: true

  @spec start_link(any) :: :ignore | {:error, any} | {:ok, pid}
  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end


  @spec init(any) :: {:ok, any}
  def init init_arg do
    Application.put_env(:tagger, :token, System.get_env("TOKEN", Application.get_env(:tagger, :token)))
    Application.put_env(:tagger, :keep_old_tags, System.get_env("KEEP_OLD_TAGS", Application.get_env(:tagger, :keep_old_tags)))

    nodes = [ node() ]
    Memento.stop
    Memento.Schema.create(nodes)
    Memento.start
    with :ok <- Memento.Table.create(Tagger.Storage, disc_copies: nodes) do
      :ok
    else
      {:error, {:already_exists, Tagger.Storage}} -> :ok
    end
    Logger.warn("Started Tagger")

    {:ok, init_arg}
  end


  @spec getall :: list
  def getall do
    {:ok , q} = Memento.transaction fn -> Memento.Query.all(Tagger.Storage) end
    for n <- q, do: Map.delete(Map.from_struct(n), :__meta__)

  end

  @doc """
  A post hendler
  Memento.Query.write(%Tagger.Storage{envkind: "sandbox", namespace: "bot", name: "image", tag: "blabla"})
  dublicates will be deleted
  """
  @spec post(%Tagger.Storage{}) :: %{}
  def post image do

    #clean record if exist
    if Application.get_env(:tagger, :keep_old_tags) != "true" do
      {:ok, f} = Memento.transaction fn -> Memento.Query.select(
        Tagger.Storage, [{:==, :envkind, image.envkind}, {:==, :namespace, image.namespace}, {:==, :name, image.name}]) end
        if length(f) > 0 do
          Logger.debug("Tagger.Storage: deleting #{inspect f}" )
          Memento.transaction fn -> Memento.Query.delete_record(hd(f)) end
        end
    end

      Logger.debug("Tagger.Storage: writing #{inspect image}" )
    {:ok, q} = Memento.transaction fn -> Memento.Query.write(image) end
    Map.delete(Map.from_struct(q), :__meta__)
  end

  @doc """
  Memento query searching by envkind
  """
  defp select guards do
    {:ok, q} = Memento.transaction fn -> Memento.Query.select(Tagger.Storage, guards) end
    for n <- q, do: Map.delete(Map.from_struct(n), :__meta__)
  end

  @spec get(%{:envkind => any, optional(any) => any}) :: list
  def get params do
    case params do
      %{envkind: envkind, namespace: namespace, name: name} ->
        guards = [{:==, :envkind, envkind}, {:==, :namespace, namespace}, {:==, :name, name}]
        select(guards)
      %{envkind: envkind, namespace: namespace} ->
        guards = [{:==, :envkind, envkind}, {:==, :namespace, namespace}]
        select(guards)
      %{envkind: envkind, name: name} ->
        guards = [{:==, :envkind, envkind}, {:==, :name, name}]
        select(guards)
      %{envkind: envkind} ->
        guards = {:==, :envkind, envkind}
        select(guards)
    end
  end

  @spec delete(any) :: {:error, any} | {:ok, any}
  def delete params do
    Memento.transaction fn -> Memento.Query.delete(Tagger.Storage, params) end
  end

  @spec info :: %{
          db_nodes: any,
          erlang_memory_usage_mb: float,
          extra_db_nodes: any,
          transaction_commits: any,
          transaction_failures: any
        }
  def info do
    %{
      transaction_commits: Memento.system(:transaction_commits),
      transaction_failures: Memento.system(:transaction_failures),
      db_nodes: Memento.system(:db_nodes),
      extra_db_nodes: Memento.system(:extra_db_nodes),
      erlang_memory_usage_mb: Float.round(:erlang.memory(:total) / 1024 / 1024, 2)
    }
  end

  @spec backup :: :ok
  def backup do
    d = DateTime.utc_now
    date = "#{d.year}-#{d.month}-#{d.day}"
    path = Application.get_env(:mnesia, :dir) ++ '-backup-' ++ to_charlist(date)
    :mnesia.backup(path)
    Logger.info("Created mnesia backup: #{inspect path}")
  end

  @spec restore(any) :: :ok
  @doc """
  Restoring Mnesia from backup file, just put the path
  """
  def restore path do
    p = to_charlist(path)
    {:atomic, _} = :mnesia.restore(path, [{:default_op, :recreate_tables}])
    Logger.info("Restored mnesia from: #{inspect p}")
  end

end
