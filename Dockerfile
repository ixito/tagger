FROM elixir:1.12-alpine as build

RUN mkdir /app
COPY . /app
WORKDIR /app

# ENV RELX_REPLACE_OS_VARS true
RUN rm -rf _build
RUN mix local.hex --force && mix local.rebar --force
# TESTING
RUN MIX_ENV=test mix deps.get && mix test
# RELEASE
ENV MIX_ENV=prod
RUN mix deps.get --only prod && mix release


FROM elixir:1.12-alpine
RUN mkdir -p /app/_build/prod
COPY --from=build /app/_build/prod /app/_build/prod
EXPOSE 8880
WORKDIR /app
ENV MIX_ENV=prod
ENV BASE_TAG $TAG

ENTRYPOINT [ "_build/prod/rel/tagger/bin/tagger", "start" ]
