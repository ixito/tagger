defmodule Tagger.MixProject do
  use Mix.Project

  def project do
    [
      app: :tagger,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :memento],
      mod: {Tagger.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:memento, "~> 0.3.1"},
      {:plug_cowboy, "~> 2.4.1"},
      {:jason, "~> 1.1"},
      {:tesla, "~> 1.4.1"},
      {:maru, "~> 0.13"},
      {:plug_crypto, "~> 1.2"},
      {:mock, "~> 0.3.0", only: :test}
    ]
  end
end
