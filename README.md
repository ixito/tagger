# Tagger

Save your tags

Object hierarchy: ENVKIND -> NAMESPACE -> NAME -> TAG

## Example

    envkind: "sandbox"              <- could be any envkind
    id: 5                           <- auto-generated id by tagger
    namespace: "quarantine"         <- k8s namespace for current image 
    name: "quarantine-postfix"      <- name of current image
    tag: "develop-f9559c95-16"      <- tag for image e.g. quarantine-postfix:develop-f9559c95-16


Written in Elixir, used [Mnesia](https://erlang.org/doc/man/mnesia.html) embedded database as tag store

## Build

    docker build -t tagger .
    docker run  --rm -p 8880:8880 -t tagger tagger

You can set an access token by env TOKEN, by default it's "testing"

## Httpie examples:

posting tags

    http POST 127.0.0.1:8880/images name=myimage tag=mytag namespace=test envkind=sandbox token=testing

getting one tag (envkind, namespace, name)

    http GET 127.0.0.1:8880/images?name=myimage\&namespace=test\&envkind=sandbox

getting tags by namespace

    http GET 127.0.0.1:8880/images?namespace=test\&envkind=sandbox

gatting tags by envkind

    http GET 127.0.0.1:8880/images?envkind="sandbox"

getting all tags

    http GET 127.0.0.1:8880/getall

deleting tags by id

    http DELETE 127.0.0.1:8880/images id=1 token="testing"


## Curl example:
    curl -i -H "Content-Type: application/json" --request POST --data '{"name":"elk-squeezer","tag":"develop-755b7af6-40", "envkind":"sandbox", "namespace":"squeezer", "token":"testing" } ' 127.0.0.1:8880/images


## env variable options:

    KEEP_OLD_TAGS="true" to restrict auto deleting tags (default is "false")
    TOKEN="sometoken" to set write access token
