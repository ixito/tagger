defmodule TaggerKeepOldTagsTest do
  use ExUnit.Case, async: false
  import Mock

  use Tesla
  plug Tesla.Middleware.BaseUrl, "http://127.0.0.1:8880"
  plug Tesla.Middleware.JSON
  plug Tesla.Middleware.Query, [envkind: "sandbox"]

  setup  do
    assert Mix.env == :test
    {:atomic, :ok}  = :mnesia.clear_table(Tagger.Storage)
    :ok
  end

  test "keep old tags test" do
    Application.put_env(:tagger, :keep_old_tags, "true")
    data = %{envkind: "sandbox", name: "image", namespace: "bot", tag: "megatag", token: "testing"}
    {:ok, response} = post("/images", data)
    assert response.status == 201
    {:ok, response2} = post("/images", data)
    assert response2.status == 201
    {:ok, response2} = post("/images", data)
    assert response2.status == 201
    assert length(Tagger.Storage.getall()) == 3
  end
end
