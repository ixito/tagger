use Mix.Config
config :logger, level: :info

config :mnesia,
  dir: '.mnesia/#{Mix.env}/#{node()}'

config :tagger,
  token: "testing",
  keep_old_tags: "false"
