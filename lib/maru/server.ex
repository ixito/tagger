defmodule Tagger.Server do
  use Maru.Server, otp_app: :maru
end

defmodule Tagger.Router.Images do
  require Logger
  use Tagger.Server

  namespace :info do
    get do
      result = Tagger.Storage.info()
      conn
        |> put_status(200)
        |> json(result)
    end
  end

  namespace :getall do
    get do
      result = Tagger.Storage.getall()
      conn
        |> put_status(200)
        |> json(result)
    end
  end

  namespace :metrics do
    get do
      {df, 0} = System.cmd("df", [to_string(Application.get_env(:mnesia, :dir))])
      free_space = round(String.to_integer(hd(Enum.take(String.split(String.replace(df, ~r/\s+/, " "), " "), -4))) / 1024 )
      records = length(Tagger.Storage.getall())
      prometheus_message =
      """
      # HELP tagger_records_in_db Number of recors in Mnesia database.
      # TYPE tagger_records_in_db gauge
      tagger_records_in_db #{records}
      # HELP tagger_db_free_space Free space of megabytes for database.
      # TYPE tagger_db_free_space gauge
      tagger_db_free_space #{free_space}

      """
      conn
        |> put_status(200)
        |> text(prometheus_message)
    end
  end

  namespace :images do


    params do
      requires :id, type: Integer
      requires :token, type: String, values: [Application.get_env(:tagger, :token)]
    end
    delete do
      :ok = Tagger.Storage.delete(params[:id])
      conn
      |> put_status(202)
      |> text("If id:#{params[:id]} exists I will delete it")
    end

    params do
      optional :namespace, type: String, regexp: ~r/^[[:alnum:]-]+$/
      optional :name, type: String
      requires :envkind, type: String
    end
    get do
      result = Tagger.Storage.get(params)
      conn
      |> put_status(length(result) == 0 && :not_found || 200)
      |> json(result)
    end

    params do
      requires :envkind, type: String, regexp: ~r/^[[:alnum:]-]+$/
    end
    get do
      result = Tagger.Storage.get(params[:envkind])
      conn
      |> put_status(length(result) == 0 && :not_found || 200)
      |> json(result)
    end

    params do
      requires :tag, type: String
      requires :name, type: String
      requires :namespace, type: String, regexp: ~r/^[[:alnum:]-]+$/
      requires :envkind, type: String
      requires :token, type: String, values: [Application.get_env(:tagger, :token)]
    end
    post do
      result = Tagger.Storage.post(%Tagger.Storage{
        envkind: params[:envkind], namespace: params[:namespace],
        name: params[:name], tag: params[:tag]})
      conn
        |> put_status(201)
        |> json(result)
    end
  end
end



defmodule Tagger.Router.Homepage do
  use Tagger.Server
  get do
    conn
      |> redirect("/images", permanent: true)
  end
end

defmodule Tagger.API do
  use Tagger.Server

  plug Plug.Parsers,
    pass: ["application/json"],
    json_decoder: Jason,
    parsers: [:urlencoded, :json, :multipart]

  mount Tagger.Router.Homepage
  mount Tagger.Router.Images

  rescue_from :all do
    conn
    |> put_status(500)
    |> json("Internal Server Error")
  end
end
