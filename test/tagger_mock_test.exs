defmodule TaggerMockTest do
  use ExUnit.Case, async: false
  import Mock

  use Tesla
  plug Tesla.Middleware.BaseUrl, "http://127.0.0.1:8880"
  plug Tesla.Middleware.JSON
  plug Tesla.Middleware.Query, [envkind: "sandbox"]

  test "mock test /metrics free space counter" do
    size = 63157480
    calculated_size = round(size / 1024)
    result = {"Filesystem  1K-blocks Used Available Use% Mounted on\n/dev/mapper/ubuntu--mate--vg-root 242791844 167231472  #{size}  73% /\n", 0}
    with_mock System, [cmd: fn(_command, _path) -> result end] do
      {:ok, response} = get("/metrics")
      assert response.status == 200
      assert String.contains?(response.body, "tagger_db_free_space #{calculated_size}")
    end
  end

  test "mock test /metrics records in db counter" do
    mock_result = [
      %{envkind: "sandbox", id: 1, name: "cvcvc", namespace: "testt", tag: "adasd1"},
      %{envkind: "sandbox", id: 2, name: "myimaged", namespace: "test", tag: "mytagb"},
      %{envkind: "sandbox", id: 3, name: "myimagedv", namespace: "test", tag: "mytagbvz"},
      %{envkind: "develop", id: 4, name: "myimagedv", namespace: "test", tag: "mytagbvz"}
    ]

    expected_result = length(mock_result)
    with_mock Tagger.Storage, [getall: fn() -> mock_result end] do
      {:ok, response} = get("/metrics")
      assert response.status == 200
      assert String.contains?(response.body, "tagger_records_in_db #{expected_result}")
    end
  end
end
