defmodule Tagger.Application do
  use Application

  @spec start(any, any) :: {:error, any} | {:ok, pid}
  def start(_type, _args) do

    children = [
      Tagger.Storage,
      Tagger.Server
    ]

    opts = [strategy: :one_for_one, name: Tagger.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
